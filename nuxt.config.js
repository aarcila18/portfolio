export default {
    // Disable server-side rendering (https://go.nuxtjs.dev/ssr-mode)
    ssr: false,

    // Target (https://go.nuxtjs.dev/config-target)
    target: 'static',

    // Global page headers (https://go.nuxtjs.dev/config-head)
    head: {
        title: 'Andres Arcila - Portfolio 2021',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: 'Im a interactive media designer with experience on full stack development (Laraver framework) , this combination of qualities make me understand the dimentions of the proyect and be there for end to end as a product designer. In short words im a Ux/UI designer that understand the code and his structure.' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ],
        script: [
            { src: 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/gsap.min.js' }
        ]
    },

    // Global CSS (https://go.nuxtjs.dev/config-css)
    css: [],

    // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
    plugins: [],

    // Auto import components (https://go.nuxtjs.dev/config-components)
    components: true,

    // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
    buildModules: [
        // https://go.nuxtjs.dev/tailwindcss
        '@nuxtjs/tailwindcss',
    ],

    // Modules (https://go.nuxtjs.dev/config-modules)
    modules: [
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
        // https://go.nuxtjs.dev/pwa
        '@nuxtjs/pwa',
        '@nuxtjs/firebase',
        '@nuxtjs/google-fonts',
        '@neneos/nuxt-animate.css'

    ],
    googleFonts: {
        families: {
            'Montserrat': [300],
            'Open Sans': [400],
        }
    },
    firebase: {
        config: {
            apiKey: "AIzaSyCGAtFg5uuQSO_7tcKTK1AqtXCs1Op38T4",
            authDomain: "portfolio-cce7e.firebaseapp.com",
            projectId: "portfolio-cce7e",
            storageBucket: "portfolio-cce7e.appspot.com",
            messagingSenderId: "624201386571",
            appId: "1:624201386571:web:c8b6edc7c4667bce351bce"
        },
        services: {
            database: true,
            firestore: true,
        }
    },


    // Axios module configuration (https://go.nuxtjs.dev/config-axios)
    axios: {},

    // Build Configuration (https://go.nuxtjs.dev/config-build)
    build: {}
}